"use strict";

function smartHeater_popupSelected() {
  document.querySelector(".room-popup [name='state'] input").checked = selectedDevice.state;
}

function smartHeater_toggleState() {
  if(selectedDevice.state) {
    selectedDevice.state = false;
  } else {
    selectedDevice.state = true;
  }
  saveLayout();
}
