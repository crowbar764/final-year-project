"use strict";

function smartLedLamp_popupSelected() {
  document.querySelector(".room-popup [name='state'] input").checked = selectedDevice.state;
  document.querySelector(".room-popup [name='colour']").value = selectedDevice.colour;
}

function smartLedLamp_toggleState(e) {
  var device = selectedDevice;

  if(device.state) {
    device.state = false;
  } else {
    device.state = true;
  }

  saveLayout();
}

function smartLedLamp_changeColour(e) {
  var device = selectedDevice;

  device.colour = e.target.value;

  saveLayout();
}
