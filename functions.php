<?php
session_start();

$DATABASE_HOST = '127.0.0.1:3306';
$DATABASE_USER = 'root';
$DATABASE_PASS = '';
$DATABASE_NAME = 'users';
// Try and connect using the info above.
$con = mysqli_connect($DATABASE_HOST, $DATABASE_USER, $DATABASE_PASS, $DATABASE_NAME);
if ( mysqli_connect_errno() ) {
  echo 'Connection error: '.mysqli_connect_error();
}

$parameters = json_decode(file_get_contents('php://input'));

call_user_func($parameters->function);

/*
Login Attempt

Required:
parameters -> username - Profile username.
parameters -> password - Profile password.

Checks if there is an account with the supplied credentials and if so, logs in.
*/
function loginAttempt() {
  global $con;
  global $parameters;

  if ($stmt = $con->prepare('SELECT userid, password FROM usertable WHERE username = ?')) {
  	$stmt->bind_param('s', $parameters->username);
  	$stmt->execute();
  	$stmt->store_result();

    if ($stmt->num_rows > 0) {
  	$stmt->bind_result($id, $password);
  	$stmt->fetch();
  	if (password_verify($parameters->password, $password)) {
  		session_regenerate_id();
  		$_SESSION['loggedin'] = TRUE;
  		$_SESSION['name'] = $parameters->username;
  		echo 'success';
  		exit;
  	} else {
  		echo 'Incorrect password';
  	}
  } else {
  	echo 'Incorrect username';
  }

  	$stmt->close();
  }
}

/*
Signup Attempt

Required:
parameters -> username - Desired username.
parameters -> password - Desired password.
parameters -> passwordconfirm - Desired password's double.

Checks if there is an account with the supplied credentials and if not, creates one and logs in.
*/
function signupAttempt() {
  global $con;
  global $parameters;

  if ($stmt = $con->prepare('SELECT username FROM usertable WHERE username = ?')) {
  	$stmt->bind_param('s', $parameters->username);
  	$stmt->execute();
  	$stmt->store_result();

    if ($stmt->num_rows > 0) {
      echo 'Username already in use';
  } else {

    if($parameters->password === $parameters->passwordconfirm) {
      $username = $parameters->username;
      $password = password_hash($parameters->password, PASSWORD_DEFAULT);
      $lastDeviceId = -1;
      $baseLayout = '{"meta":{"width":5,"rooms":[]},"levels":[[]]}';
      $baseUserdata = '{"order":["layout"],"layout":{"moduletype":"layout","scripts":["layout", "layout-popup"],"moduletitle":"","extraclasses":"full-width"}}';

      $query = $con->prepare('INSERT INTO usertable (username, password, lastdeviceid, layout, userdata) VALUES (?, ?, ?, ?, ?)');
      $query->bind_param('ssiss', $username, $password, $lastDeviceId, $baseLayout, $baseUserdata);
      $query->execute();

      session_regenerate_id();
      $_SESSION['loggedin'] = TRUE;
      $_SESSION['name'] = $parameters->username;
      echo 'success';
      exit;
    } else {
      echo 'Passwords do not match';
    }
  }

  	$stmt->close();
  }
}

/*
Download Layout

Returns the current account's JSON layout data.
*/
function downloadLayout() {
  global $con;
  global $parameters;

  if ($stmt = $con->prepare('SELECT layout FROM usertable WHERE username = ?')) {
  	$stmt->bind_param('s', $_SESSION['name']);
  	$stmt->execute();
  	$stmt->store_result();

    if ($stmt->num_rows > 0) {
  	$stmt->bind_result($layout);
  	$stmt->fetch();

    echo $layout;
  } else {
  	echo 'User not found with name'.$_SESSION['name'];
  }

  	$stmt->close();
  }
}

/*
Save Layout

Required:
parameters -> layout - JSON data for layout.

Updates the user's database profile with the supplied data.
*/
function saveLayout() {
  global $con;
  global $parameters;

  if ($stmt = $con->prepare('UPDATE usertable SET layout = ? WHERE username = ?')) {
    $stmt->bind_param('ss', $parameters->layout, $_SESSION['name']);
    $stmt->execute();
    $stmt->store_result();
    $stmt->close();
  } else {
    echo 'fail?';
  }
}

/*
Get Last Device ID

Returns the id for the last device used by the logged in account.
*/
function getLastDeviceId() {
  global $con;
  global $parameters;

  if ($stmt = $con->prepare('SELECT lastdeviceid FROM usertable WHERE username = ?')) {
    $stmt->bind_param('s', $_SESSION['name']);
    $stmt->execute();
    $stmt->store_result();

    if ($stmt->num_rows > 0) {
    $stmt->bind_result($lastDeviceId);
    $stmt->fetch();

    echo $lastDeviceId;
  } else {
    echo 'fail?';
  }

    $stmt->close();
  }
}

/*
Set Last Device ID

Required:
Parameters -> id - new last device ID.

Sets the id for the last device used by the logged in account.
*/
function setLastDeviceId() {
  global $con;
  global $parameters;

  if ($stmt = $con->prepare('UPDATE usertable SET lastdeviceid = ? WHERE username = ?')) {
    $stmt->bind_param('is', $parameters->id, $_SESSION['name']);
    $stmt->execute();
    $stmt->store_result();
    $stmt->close();
  } else {
    echo 'fail?';
  }
}

/*
Download User Data Single

Required:
Parameters -> dataitem - the name of the data item desired.

Gets the specified data item from the current account's profile.
*/
function downloadUserDataSingle($dataitem) {
  global $con;

  if ($stmt = $con->prepare('SELECT userdata FROM usertable WHERE username = ?')) {
    $stmt->bind_param('s', $_SESSION['name']);
    $stmt->execute();
    $stmt->store_result();

    if ($stmt->num_rows > 0) {
    $stmt->bind_result($userdata);
    $stmt->fetch();

    // Converts userdata to readable JSON, gets desired value, converts back to string
    return json_encode(json_decode($userdata)->$dataitem);
  } else {
    echo 'User not found with name'.$_SESSION['name'];
  }

    $stmt->close();
  }
}


/*
Download User Data

Gets all module user data from the account's profile. A less specific version of the above function.
*/
function downloadUserData() {
  global $con;

  if ($stmt = $con->prepare('SELECT userdata FROM usertable WHERE username = ?')) {
  	$stmt->bind_param('s', $_SESSION['name']);
  	$stmt->execute();
  	$stmt->store_result();

    if ($stmt->num_rows > 0) {
  	$stmt->bind_result($userdata);
  	$stmt->fetch();

    echo $userdata;
  } else {
  	echo 'User not found with name'.$_SESSION['name'];
  }

  	$stmt->close();
  }
}

/*
Set User Data

Required:
Parameters -> userdata - The replacement data

Updates the account profile's module data.
*/
function setUserData() {
  global $con;
  global $parameters;

  if ($stmt = $con->prepare('UPDATE usertable SET userdata = ? WHERE username = ?')) {
  	$stmt->bind_param('ss', $parameters->userdata, $_SESSION['name']);

    $stmt->execute();
    $stmt->store_result();
    $stmt->close();
  }
}

// Components for the device interactive menus
function component_switch($camelName, $dataName, $label) {
  include("templates/components/switch.php");
}

function component_select($camelName, $dataName, $label, $options) {
  include("templates/components/select.php");
}
