<?php
  session_start();
?>

<html lang="en">
  <head>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" type="text/css" href="css/styles.css">
  <script src="js/ajax.js"></script>
  <title>Dashboard</title>
  </head>
  <body>

    <?php
    if (isset($_SESSION['loggedin'])) {
      include 'templates/admin.php';
    }
    ?>

    <div class="navbar">
      <div class="background"></div>
      <div class="outer-wrapper">
        <div class="inner-wrapper">
          <div class="logo-wrapper">
            <a class="logo" href="index.php"></a>
          </div>

          <div class="layout-editor-hotbar hotbar">
            <a class="hotbar-button editor-hotbar-back background-image green-button" href="index.php"></a>
            <button class="hotbar-button editor-hotbar-save background-image green-button" onclick="saveLayout()" disabled></button>
            <button class="hotbar-button editor-hotbar-add-level background-image green-button" onclick="addLevel()"></button>
          </div>

          <div class="dashboard-hotbar hotbar">
            <button class="hotbar-button dashboard-hotbar-togglemode background-image green-button" onclick="toggleModuleEditMode()"></button>
          </div>


          <?php
            if (isset($_SESSION['loggedin'])) {
              echo '
                <div class="user-information">
                  <p class="name">'.$_SESSION['name'].'</p>
                  <a class="logout" href="logout.php">Logout</a>
                </div>
              ';
            }
          ?>

        </div>
      </div>
    </div>

    <div class="outer-wrapper">
      <div class="inner-wrapper">
