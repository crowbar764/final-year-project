<script src="js/layout.js"></script>
<script src="js/layout-popup.js"></script>

<div class="layout-wrapper">

  <div class="layout-reel"></div>
  <div class="layout-controls">
    <button class="background-image green-button next " onclick="nextLevel()"></button>
    <button class="background-image green-button prev " onclick="previousLevel()"></button>
    <a class="background-image green-button layout-options" href="layout-editor.php"></a>
  </div>

</div>
