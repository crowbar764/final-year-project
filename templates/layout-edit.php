<script> document.querySelector("body").classList.add("layout-editor-body"); </script>
<script src="js/layout.js"></script>
<script src="js/layout-popup.js"></script>
<script src="js/layout-editor-room-list.js"></script>

<div class="layout-wrapper layout-editor">
  <?php include 'templates/layout-editor-room-list.php'; ?>
  <?php include 'templates/layout-editor-unplaced-devices.php'; ?>
  <div class="layout-reel"></div>
</div>
