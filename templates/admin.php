<script src="js/admin.js"></script>

<div class="admin-panel">
  <div class="admin-toggle-menu" onclick="admin_togglePanel()">
    <p>Admin tools</p>
    <div class="background-image"></div>
  </div>
  <div class="function">
    <button onclick="loadExampleLayout()">Example layout</button>
    <div class="tooltip background-image"><div>Loads this account with an example house 'layout'. (Will overwrite existing 'layout')</div></div>
  </div>
  <div class="function">
    <button onclick="admin_addNewDevice()">Add new device</button>
    <div class="tooltip background-image"><div>Adds a new device to the user's account.</div></div>
  </div>
  <div class="function">
    <button onclick="admin_populateModuleData()">Example modules</button>
    <div class="tooltip background-image"><div>Loads this account with a few example modules for the dashboard.</div></div>
  </div>
  <div class="function">
    <button onclick="getLayoutString()">Get layout string</button>
    <div class="tooltip background-image"><div>Pastes the current user's layout JSON in console.</div></div>
  </div>
</div>
