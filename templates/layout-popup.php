<div class="room-popup">
  <div class="room-popup-content">
    <div class="room-popup-content-reel">
      <div class="room-popup-content-left">
        <div class="title-bar">
          <select id="room-popup-name" class="title" onchange="cellRoomChange()"></select>
          <button class="close" type="button" onclick="closePopup()"></button>
        </div>
        <div class="popup-room-actions">
          <button class="make-label green-button" onclick="setNewLabel(event)">Set as label</button>
        </div>
      </div>
      <div class="room-popup-content-right">
        <div class="title-bar">
          <div class="back" onclick="popupUnselectDevice()"></div>
          <p id="room-popup-device-name" class="title">DEVICE</p>
          <button class="close" type="button" onclick="closePopup()"></button>
        </div>
        <div class="device-interactions"></div>
      </div>
    </div>
  </div>
</div>
