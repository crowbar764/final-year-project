<div class="interaction-select interaction-split">
  <label for="colour"><?php echo $label ?></label>
  <select name="colour" onchange="<?php echo $camelName ?>_changeColour(event)">
    <?php echo $options ?>
  </select>
</div>
