<div class="interaction-switch interaction-split">
  <label for="state"><?php echo $label ?></label>
  <div class="switch-spacer">
    <label name=<?php echo $dataName ?> class="switch">
      <input type="checkbox" onchange="<?php echo $camelName ?>_toggleState(event)">
      <span class="slider"></span>
    </label>
  </div>
</div>
