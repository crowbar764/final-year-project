<figure class="graph" data-graphdata='<?php echo json_encode($moduleData->graphdata) ?>'>
  <div class="graph-x">
    <p class="label-limit">2</p>
    <p class="label-half">1</p>
    <p>0</p>
  </div>
  <div class="graph-content">
    <ul class="graph-data"></ul>
  </div>
  <div class="graph-y">
  </div>
</figure>
