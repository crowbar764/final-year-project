"use strict";

var roomList, unplacedDevicesList, currentDevice;

/*
Update Room List

Render the list of rooms and devices from local memory.
*/
function updateRoomList() {
  roomList.innerHTML = "";
  unplacedDevicesList.innerHTML = "";
  unplacedDevicesList.parentElement.parentElement.parentElement.parentElement.classList.remove("visible");

  var template = document.querySelector(".template-list-item .list-item");

  for(var i = 0; i < mapData.meta.rooms.length; i++) {

    // If room has been deleted
    if(mapData.meta.rooms[i] == null) {
      continue;
    }

    var roomNameRaw = mapData.meta.rooms[i].replace(/["']/g, "&apos;"); // Avoids escaping quotes.
    var roomNameRaw = roomNameRaw.replace(/[ ]/g, "&nbsp;"); // Avoids spaces which template literals don't like.

    var listItem = document.createElement("DIV");
    listItem.innerHTML = `
    <div class="list-item-header">
    <input type='text' value='${roomNameRaw}' oninput='roomNameChanged(event)'></input>
    <button class='green-button background-image update-room-name' onClick='updateRoomName(event)' disabled></button>
    <button class='green-button background-image remove-room-name' onClick='removeRoomName(event)'></button>
    <button class='green-button background-image expand-list-item' onClick='expandRoomListItem(event)' disabled></button>
    </div>
    <div class="list-item-collapsible">
    </div>
    `;
    listItem.classList.add("list-item");
    listItem.dataset.roomid = i;

    roomList.appendChild(listItem);
  }

  addDevices();
  roomList.innerHTML += "<button class='green-button add-room-name' onclick='addRoomName()'>Add new</button>";
}

/*
Add Devices

Render device markup and add to room list.
*/
function addDevices() {
  for(var i = 0; i < mapData.devices.length; i++) {
    var currentListItemCollapsible = roomList.querySelector("[data-roomid='" + mapData.devices[i].roomid + "'] .list-item-collapsible");

    var deviceMarkup = document.createElement("DIV");
    deviceMarkup.classList.add("device");
    deviceMarkup.dataset.deviceid = mapData.devices[i].deviceid;

    deviceMarkup.innerHTML = `
    <div class="device-header">
      <h3 class="device-name">${mapData.devices[i].niceName}</h3>
      <select class="device-room-change" onchange="deviceRoomChange(event)">${roomOptions}</select>
      <button class="green-button background-image remove-device" onclick="removeDevice(event)"></button>
    </div>
    `;

    if(currentListItemCollapsible) {
      currentListItemCollapsible.appendChild(deviceMarkup);
      document.querySelector("[data-deviceid='" + mapData.devices[0].deviceid + "'] select").value = "Closet";

      roomList.querySelector(".list-item[data-roomid='" + mapData.devices[i].roomid + "'] .expand-list-item").disabled = false;
    } else {
      unplacedDevicesList.parentElement.parentElement.parentElement.parentElement.classList.add("visible");
      unplacedDevicesList.appendChild(deviceMarkup);
      return;
    }
  }
}

/*
Add Room Name

Adds a new room to the user's profile (blank).
*/
function addRoomName() {
  mapData.meta.rooms.push("");
  createLayoutFromLocal();
}

/*
Remove Room Name

Required:
e - Click event.

Remove a room from the user's list of rooms.
*/
function removeRoomName(e) {
  mapData.meta.rooms[e.target.parentElement.parentElement.dataset.roomid] = null;

  for(var level = 0; level < mapData.levels.length; level++) {
    for(var cell = 0; cell < mapData.levels[level].length; cell++) {
      if(mapData.levels[level][cell].roomid - 1 == e.target.parentElement.parentElement.dataset.roomid) {
        mapData.levels[level][cell].roomid = 0;
      }
    }
  }
  createLayoutFromLocal();
}

/*
Room Name Changed

Required:
e - Select change event.

When the user modifies the room name text box.
*/
function roomNameChanged(e) {
  var roomId = e.target.parentElement.parentElement.dataset.roomid;
  var roomButton = roomList.querySelector(".list-item[data-roomid='" + roomId + "'] .update-room-name");
  if(e.target.value == mapData.meta.rooms[roomId]) {
    roomButton.disabled = true;
  } else {
    roomButton.disabled = false;
  }
}

/*
Update Room Name

Required:
e - Button event.

When the user confirms an edit to a room name, apply this change to their profile and the layout.
*/
function updateRoomName(e) {
  e.target.disabled = true;
  var roomId = e.target.parentElement.parentElement.dataset.roomid;
  var newRoomName = roomList.querySelector(".list-item[data-roomid='" + roomId + "'] input").value;
  mapData.meta.rooms[roomId] = newRoomName;
  createLayoutFromLocal();

  layoutReel.children[currentLevel].classList.remove("raised");
}

/*
Expand Room List Item

Required:
e - Button event.

The user clicks the dropdown button to show room devices.
*/
function expandRoomListItem(e) {
  if(e.target.parentElement.parentElement.classList.contains("active")) {
    e.target.parentElement.parentElement.classList.remove("active");
  } else {
    e.target.parentElement.parentElement.classList.add("active");
  }
}

/*
Edit Interactions Downloaded

When device controls are downloaded for the room list.
*/
function editInteractionsDownloaded() {
  var device = mapData.devices[currentDevice];

  var deviceMarkup = document.createElement("DIV");
  deviceMarkup.classList.add("device");
  deviceMarkup.dataset.deviceid = device.deviceid;

  deviceMarkup.innerHTML = `
      <div class="device-header">
        <h3 class="device-name">${device.niceName}</h3>
        <select class="device-room-change" onchange="deviceRoomChange(event)">${roomOptions}</select>
        <button class="green-button background-image remove-device" onclick="removeDevice(event)"></button>
      </div>
      <div class="device-extras">${this.responseText}</div>
      `;

  var currentListItemCollapsible = roomList.querySelector(".list-item[data-roomid='" + device.roomid + "'] .list-item-collapsible");

  if(currentListItemCollapsible) {
    currentListItemCollapsible.appendChild(deviceMarkup);
    currentListItemCollapsible.querySelector("[data-deviceid='" + device.deviceid + "'] select").value = mapData.meta.rooms[device.roomid];
    currentListItemCollapsible.querySelector("[data-deviceid='" + device.deviceid + "'] select").classList.add(device.deviceid);

    roomList.querySelector(".list-item[data-roomid='" + device.roomid + "'] .expand-list-item").disabled = false;
  } else {
    unplacedDevicesList.parentElement.parentElement.parentElement.parentElement.classList.add("visible");
    unplacedDevicesList.appendChild(deviceMarkup);
    return;
  }
}

/*
Device Room Change

Required:
e - Select change event.

Changes which room the device is linked to.
*/
function deviceRoomChange(e) {
  var device = getDeviceFromId(e.target.parentElement.parentElement.dataset.deviceid);

  device.roomid = mapData.meta.rooms.indexOf(e.target.value);
  createLayoutFromLocal();
}

/*
Remove Device

Required:
e - Click event.

Removes the device from the user's profile.
*/
function removeDevice(e) {
  var deviceId = e.target.parentElement.parentElement.dataset.deviceid;
  var index = mapData.devices.indexOf(getDeviceFromId(deviceId));

  mapData.devices.splice(index, 1);
  createLayoutFromLocal();
}

function initRoomList() {
  roomList = document.querySelector(".layout-editor-room-list");
  unplacedDevicesList = document.querySelector(".layout-editor-unplaced-devices .list-item .list-item-collapsible");
}
