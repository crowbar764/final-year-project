"use strict";

function admin_togglePanel() {
  var panel = document.querySelector(".admin-panel");

  if(panel.classList.contains("active")) {
    panel.classList.remove("active");
  } else {
    panel.classList.add("active");
  }
}

function loadExampleLayout() {
  var exampleString = '{"meta":{"width":5,"rooms":["Kitchen","Dining room","Lounge","Hallway 1","Closet","Study","Master bedroom","Bathroom","Sam\'s bedroom","Hallway 2","Amy\'s bedroom","Spare room","Attic"]},"levels":[[{"roomid":1},{"roomid":2,"isLabel":false},{"roomid":2,"isLabel":true},{"roomid":3},{"roomid":3,"isLabel":false},{"roomid":1,"isLabel":true},{"roomid":1},{"roomid":4},{"roomid":3,"isLabel":true},{"roomid":3},{"roomid":4},{"roomid":4},{"roomid":4,"isLabel":true},{"roomid":5,"isLabel":true},{"roomid":5},{"roomid":6},{"roomid":6,"isLabel":true},{"roomid":4},{"roomid":4},{"roomid":4}],[{"roomid":7},{"roomid":7},{"roomid":8,"isLabel":true},{"roomid":9},{"roomid":9},{"roomid":7},{"roomid":7,"isLabel":true},{"roomid":10},{"roomid":9,"isLabel":true},{"roomid":11},{"roomid":12},{"roomid":10},{"roomid":10,"isLabel":true},{"roomid":11,"isLabel":true},{"roomid":11},{"roomid":12},{"roomid":12,"isLabel":true},{"roomid":10},{"roomid":10},{"roomid":10}],[{"roomid":13},{"roomid":13},{"roomid":13},{"roomid":13},{"roomid":13},{"roomid":13},{"roomid":13},{"roomid":13,"isLabel":true},{"roomid":13},{"roomid":13},{"roomid":13},{"roomid":13},{"roomid":13},{"roomid":13},{"roomid":13},{"roomid":13},{"roomid":13},{"roomid":13},{"roomid":13},{"roomid":13}]],"devices":[{"typeid":0,"camelName":"smartLedLamp","niceName":"Smart Led Lamp","deviceid": 0,"roomid":2,"state":0,"colour":"Red"},{"typeid":1,"camelName":"smartHeater","niceName":"Smart Heater","deviceid": 1,"roomid":7}]}';

  var parameters = {function: "saveLayout", layout: exampleString};
  newAjaxRequest(parameters, function() {
    location.reload();
  });
}

function getLayoutString() {
  console.log(mapData);
}

function admin_addNewDevice() {
  addNewDevice("smartLedLamp");
}

function admin_populateModuleData() {
  var exampleData = '{"order":["energybill","gasbill","layout"],"gasbill":{"moduletype":"graph","scripts":["graph"],"moduletitle":"Gas Bill (£)","extraclasses":"rounded","graphdata":[{"value":39,"label":"1"},{"value":55,"label":"2"},{"value":13,"label":"3"}]},"energybill":{"moduletype":"graph","scripts":["graph"],"moduletitle":"Energy Bill (£)","extraclasses":"rounded","graphdata":[{"value":239,"label":"1"},{"value":23,"label":"2"},{"value":13,"label":"3"}]},"layout":{"moduletype":"layout","scripts":["layout", "layout-popup"],"moduletitle":"","extraclasses":"full-width"}}';

  var parameters = {function: "setUserData", userdata: exampleData};
  newAjaxRequest(parameters, function() {
    location.reload();
  });

}
