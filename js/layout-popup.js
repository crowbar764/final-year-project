"use strict";

var layoutPopup, roomActions, popupRoomName, popupDeviceName, selectedDevice;

/*
Update Popup Position

Requires:
e - Mouse event.

Update position of layout popup to the mouse.
*/
function updatePopupPosition(e) {
  layoutPopup.style.left = e.pageX - (layoutPopup.offsetWidth / 2);
  layoutPopup.style.top = e.pageY - (layoutPopup.offsetHeight + 32);
}

/*
Update Popup Content

Update content (labels and controls) of layout popup
*/
function updatePopupContent() {
  var makeLabelButton = layoutPopup.querySelector(".make-label");
  if(editMode) {
    makeLabelButton.classList.remove("visible");
  }

  if(selectedRoom != 0) {
    popupRoomName.value = mapData.meta.rooms[selectedRoom - 1];

    // If cell is already showing the room label.
    if(!getSelectedCell().classList.contains("is-label") && editMode) {
      makeLabelButton.classList.add("visible");
    }

  } else {
    popupRoomName.value = "None";
  }

  if(!editMode) {
    roomActions.innerHTML = "";
    for(var i = 0; i < mapData.devices.length; i++) {
      if(mapData.devices[i].roomid == selectedRoom - 1) {
        roomActions.innerHTML += `<button class='device green-button' onclick='popupSelectDevice(event)' data-deviceid='${mapData.devices[i].deviceid}'>${mapData.devices[i].niceName}</button>`;
      }
    }
  }
}

/*
Populate Popup Room Names

Set dropdown options for popup room label.
*/
function populatePopupRoomNames() {
  popupRoomName.innerHTML = roomOptions;
}

/*
Set New Label

Required:
e - Mouse event.

When the user changes the room name dropdown, update the cell.
*/
function setNewLabel(e) {
  var currentLabelCell = layoutReel.querySelector(".layout-level[data-level='" + selectedLevel + "'] .cell[data-roomid='" + selectedRoom + "'].is-label");

  // Remove old label if one exists
  if(currentLabelCell) {
    mapData.levels[selectedLevel][currentLabelCell.dataset.cellid].isLabel = false;
  }

  mapData.levels[selectedLevel][selectedCellId].isLabel = true;

  createLayoutFromLocal();
}

/*
Open Popup

Make the popup visible to the user.
*/
function openPopup() {
  layoutPopup.classList.add("active");
}

/*
Close Popup

Make the popup invisible to the user.
*/
function closePopup() {
  selectedCellId = null;
  selectedRoom = null;
  selectedLevel = null;
  layoutPopup.classList.remove("active");
  popupUnselectDevice();
  unhighlightAllCells();
}

/*
Popup Select Device

Required:
e - Click event.

Open device menu on popup
*/
function popupSelectDevice(e) {
  layoutPopup.classList.add("engaged");
  selectedDevice = getDeviceFromId(e.target.dataset.deviceid);

  popupDeviceName.innerHTML = selectedDevice.niceName;
  var parameters = {file: "/devices/" + selectedDevice.camelName + "/interactions.php"};
  newAjaxFileRequest(parameters, popupInteractionsDownloaded);
}

/*
Popup Unselect Device

Close device menu on popup
*/
function popupUnselectDevice() {
  layoutPopup.classList.remove("engaged");
}

/*
Popup Interactions Downloaded

Download the markup for the selected device and initiate any scripts.
*/
function popupInteractionsDownloaded() {
  layoutPopup.querySelector(".device-interactions").innerHTML = this.responseText;
  window[selectedDevice.camelName + "_popupSelected"](); // Calls the devices popupSelected script
}

function initPopup() {
  layoutPopup = document.querySelector(".room-popup");
  roomActions = layoutPopup.querySelector(".popup-room-actions");
  popupRoomName = document.querySelector("#room-popup-name");
  popupDeviceName =document.querySelector("#room-popup-device-name");
  if(!editMode) {
    roomActions.classList.add("visible");
  }
}
