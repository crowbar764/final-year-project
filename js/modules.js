"use strict";

var modulesContainer, modulesData, currentModule, scriptQueue, currentScript, scriptsDownloaded, moduleEditMode;

var needsRefresh = true;
var moduleRefreshHooks = [];

/*
Module Reorder

Required:
selectedModule - the dragged module ID.
hoveredModule - the mouse-up destination module ID.

When in edit mode and the user drags a module to a new, valid position, this function will save the changes to the server and the browser.
*/
function moduleReorder(selectedModule, hoveredModule) {
  var order = modulesData.order;
  var temp = order[hoveredModule];
  order[hoveredModule] = order[selectedModule];
  order[selectedModule] = temp;

    var parameters = {function: "setUserData", userdata: JSON.stringify(modulesData)};
    newAjaxRequest(parameters, function() {
      modulesInit();
    });
}

/*
Init Module

Required:
moduleId - the ID of the module to initiate.
moduleData - data of the module to initiate.

Call's a module's specific init function.
*/
function initModule(moduleId, moduleData) {
  var moduleElement = document.querySelector('[data-moduleid="' + moduleId + '"]');
  window[moduleElement.dataset.moduletype + "_init"](moduleElement, moduleData);
}

/*
Add Module Refresh Hook

Required:
moduleId - the ID of the module to add.

Adds the supplied ID to an array that will be processed on screen resize
*/
function addModuleRefreshHook(moduleId) {
  moduleRefreshHooks.push(parseInt(moduleId));
}

/*
Modules Resize

Runs when the screen resizes. Useful for modules that have displays depending on one time calculations.
*/
function modulesResize() {
  for(var i = 0; i < moduleRefreshHooks.length; i++) {
    var moduleElement = document.querySelector("[data-moduleid='" + moduleRefreshHooks[i] + "']");
    window[moduleElement.dataset.moduletype + "_resize"](moduleElement);
  }
}

/*
Load Modules

Load the markup for each module.
*/
function loadModules() {
  var moduleData = modulesData[modulesData.order[currentModule]];

  var moduleItem = document.createElement("DIV");
  moduleItem.classList.add("module", moduleData.extraclasses);
  moduleItem.dataset.moduleid = currentModule;
  moduleItem.dataset.moduletype = moduleData.moduletype;

  moduleItem.innerHTML = `<div class="background"></div>`;
  if(moduleData.moduletitle) {
    moduleItem.innerHTML += '<h3 class="module-title">' + moduleData.moduletitle + '</h3>';
  }

  var parameters = {file: "/templates/" + moduleData.moduletype + ".php"};
  newAjaxFileRequest(parameters, function() {
    moduleItem.innerHTML += this.response;

    moduleItem.addEventListener("mousedown", moduleClicked);
    modulesContainer.appendChild(moduleItem);

    for(var i = 0; i < moduleData.scripts.length; i++) {
      if(!scriptQueue.includes(moduleData.scripts[i])) {
        scriptQueue.push(moduleData.scripts[i]);
      }
    }

    currentModule++;
    if(currentModule == modulesData.order.length) {
      document.querySelector(".script-dump").innerHTML = "";
      downloadScript();
      return;
    }
    loadModules();
  });
}

/*
Download Script

Load the script(s) for each module.
*/
function downloadScript() {
  currentScript++;
  if(scriptsDownloaded || currentScript == scriptQueue.length) {
    scriptsDownloaded = true;
    initModules();
    return;
  }

  var script = document.createElement('SCRIPT');
  script.type = 'text/javascript';
  script.src = 'js/' + scriptQueue[currentScript] + '.js';
  document.querySelector(".script-dump").appendChild(script);

  script.onload = function() {
    downloadScript();
  };

}

/*
Init Modules

Runs the above init function for all modules and sets up for refresh.
*/
function initModules() {
  for(var i = 0; i < modulesData.order.length; i++) {
    var moduleData = modulesData[modulesData.order[i]];
    initModule(i, moduleData);
    addModuleRefreshHook(i);
  }
}

/*
Toggle Module Edit Mode

Allows dragging and dropping modules
*/
function toggleModuleEditMode() {
  if(moduleEditMode) {
    moduleEditMode = false;
    document.querySelector("BODY").classList.remove("module-editmode");
  } else {
    moduleEditMode = true;
    document.querySelector("BODY").classList.add("module-editmode");
  }
}

// Runs on DOM load.
function modulesInit() {
  modulesContainer = document.querySelector(".modules");
  modulesContainer.innerHTML = "";
  if(document.querySelector("BODY").classList.contains("module-editmode")) {
    moduleEditMode = true;
  }


  var parameters = {function: "downloadUserData"};
  newAjaxRequest(parameters, function() {
    modulesData = JSON.parse(this.response);
    currentModule = 0;
    currentScript = -1;
    scriptQueue = [];
    scriptsDownloaded = false;
    loadModules();
  });
}

window.addEventListener('resize', modulesResize);
document.addEventListener("DOMContentLoaded", modulesInit);
