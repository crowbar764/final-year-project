"use strict";

var formWrapper, loginForm, signupForm, loginSwitch, signupSwitch, loginErrorText, signupErrorText;

/*
Login Attempt

Required:
event - click event.

When the user tries to login with values in the fields, the valididity of their request is checked.
*/
function loginAttempt(event) {
  event.preventDefault();
  loginForm.querySelector("input[type='submit']").disabled = true;
  loginErrorText.innerHTML = "";

  var username = loginForm.querySelector(".username").value;
  var password = loginForm.querySelector(".password").value;

  var parameters = {function: "loginAttempt", "username": username, "password": password};
  newAjaxRequest(parameters, function() {
    if(this.responseText != "success") {
      loginErrorText.innerHTML = this.responseText;
      loginForm.querySelector("input[type='submit']").disabled = false;
    } else {
      window.location.href = "index.php";
    }
  });
}

/*
Signup Attempt

Required:
event - click event.

When the user tries to signup for a new account with values in the fields, the valididity of their request is checked.
*/
function signupAttempt(event) {
  event.preventDefault();
  signupForm.querySelector("input[type='submit']").disabled = true;
  signupErrorText.innerHTML = "";

  var xmlhttp = new XMLHttpRequest();
  xmlhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
    }
  }

  var username = signupForm.querySelector(".username").value;
  var password = signupForm.querySelector(".password").value;
  var password2 = signupForm.querySelector(".password2").value;

  var parameters = {function: "signupAttempt", "username": username, "password": password, "passwordconfirm": password2};
  newAjaxRequest(parameters, function() {
    if(this.responseText != "success") {
      signupErrorText.innerHTML = this.responseText;
      signupForm.querySelector("input[type='submit']").disabled = false;
    } else {
      // With the account created, we can set up a blank layout.
      var layoutString = '{"meta":{"width":5,"rooms":[]},"levels":[[{"roomid":0,"isLabel":false},{"roomid":0,"isLabel":false},{"roomid":0,"isLabel":false},{"roomid":0,"isLabel":false},{"roomid":0,"isLabel":false}]],"devices":[]}';

      var parameters = {function: "saveLayout", layout: layoutString};
      newAjaxRequest(parameters, function() {
        window.location.href = "index.php";
      });
    }
  });
}

/*
Select Login Form

Switches signup form to login mode.
*/
function selectLoginForm() {
  formWrapper.classList.remove("shifted");
  signupSwitch.disabled = false;
  loginSwitch.disabled = true;
}

/*
Select Signup Form

Switches login form to signup mode.
*/
function selectSignupForm() {
  formWrapper.classList.add("shifted");
  loginSwitch.disabled = false;
  signupSwitch.disabled = true;
}

function loginInit() {
  formWrapper = document.querySelector(".form-wrapper");
  loginForm = document.querySelector(".login-form");
  signupForm = document.querySelector(".signup-form");
  loginSwitch = document.querySelector(".form-selection-login");
  signupSwitch = document.querySelector(".form-selection-signup");
  loginErrorText = loginForm.querySelector(".error");
  signupErrorText = signupForm.querySelector(".error");
}

document.addEventListener("DOMContentLoaded", loginInit);
