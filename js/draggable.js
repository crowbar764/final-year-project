"use strict";

var selectedModule, moduleGhost;

/*
Module Clicked

Requires:
e - The mouse event.

A module drag is initiated if the condition are right
*/
function moduleClicked(e) {

  if(!moduleEditMode) {
    return;
  }

  selectedModule = e.target;
  while(!selectedModule.classList.contains("module")) {
    selectedModule = selectedModule.parentElement;
  }

  moduleGhost.style.display = "block";
  moduleGhost.style.width = selectedModule.offsetWidth;
  moduleGhost.style.height = selectedModule.offsetHeight;
  var modulePosition = selectedModule.getBoundingClientRect();
  var offsetX = e.clientX - modulePosition.x - window.pageXOffset;
  var offsetY = e.clientY - modulePosition.y - window.pageYOffset;
  moduleGhost.style.transform = "translate(-" + offsetX + "px,-" + offsetY + "px)";
  mouseMove(e);

  selectedModule.classList.add("ghosted");
}

/*
Module Move

Requires:
e - The mouse event.

The module drag ghost is updated on mouse move.
*/
function mouseMove(e) {
  if(selectedModule) {
    moduleGhost.style.top = e.clientY;
    moduleGhost.style.left = e.clientX;
  }
}

/*
Module Released

Requires:
e - The mouse event.

Depending on conditions, a module may swap positions now from the drag.
*/
function mouseRelease(e) {
  if(!selectedModule) {
    return;
  }

  moduleGhost.style.display = "none";
  var hoveredElement = document.elementFromPoint(e.clientX, e.clientY);

  if(hoveredElement) {
    while(!hoveredElement.classList.contains("module") && hoveredElement.parentElement) {
      hoveredElement = hoveredElement.parentElement;
    }

    if(hoveredElement.classList.contains("module")) { //Released over module
      var selectedModuleId = parseInt(selectedModule.dataset.moduleid);
      var hoveredModuleId = parseInt(hoveredElement.dataset.moduleid);
      moduleReorder(selectedModuleId, hoveredModuleId);
    }
  }
  selectedModule.classList.remove("ghosted");
  selectedModule = null;
}

function draggableInit() {
  moduleGhost = document.querySelector(".module-ghost");
  document.addEventListener("mousemove", mouseMove);
  document.addEventListener("mouseup", mouseRelease);
}

document.addEventListener("DOMContentLoaded", draggableInit);
