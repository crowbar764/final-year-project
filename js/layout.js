"use strict";

var editMode, mapData, oldMapData, layoutReel, roomOptions, selectedRoom,
selectedCellId, selectedLevel, currentLevel, nextButton,
prevButton, needsSaving, saveButton, loadedExtras, lastIdUsed;

/*
Highlight Selected Cells

Show the currently selected room as highlighted
*/
function highlightSelectedCells() {
  if(editMode) {
    getSelectedCell().classList.add("hover");
  } else {
    var selectedCell = layoutReel.querySelectorAll(".cell[data-roomid='" + getSelectedCell().dataset.roomid + "']");

    for(var i = 0; i < selectedCell.length; i++) {
      selectedCell[i].classList.add("hover");
    }
  }
}

/*
Unhighlight all cells

Remove the highlight effect from all layout cells.
*/
function unhighlightAllCells() {
  var highlightedCells = layoutReel.querySelectorAll(".cell.hover");
  for(var i = 0; i < highlightedCells.length; i++) {
    highlightedCells[i].classList.remove("hover");
  }
}

/*
Get Selected Cell

Returns an element reference to the currently selected cell.
*/
function getSelectedCell() {
  return layoutReel.querySelector(".layout-level[data-level='" + selectedLevel + "'] .cell[data-cellid='" + selectedCellId + "']");
}

/*
Cell Room Change

When the user uses the popup to select a new room name for the selected cell, update everything.
*/
function cellRoomChange() {
  var newRoomId = mapData.meta.rooms.indexOf(popupRoomName.value) + 1;
  mapData.levels[selectedLevel][selectedCellId].roomid = newRoomId;
  mapData.levels[selectedLevel][selectedCellId].isLabel = false;
  createLayoutFromLocal();

  var newSelectedCell = layoutReel.querySelector(".layout-level[data-level='" + selectedLevel + "'] .cell[data-cellid='" + selectedCellId + "']");
  newSelectedCell.classList.add("hover");
  selectedRoom = newSelectedCell.dataset.roomid;
  updatePopupContent();
}

/*
Update Map Borders

Render CSS borders on cells where nessesary.
*/
function updateMapBorders() {
  var mapWidth = mapData.meta.width;


  // For each level
  for(var li = 0; li < mapData.levels.length; li++) {
    var cells = layoutReel.children[li].querySelectorAll(".cell");

    for(var i = 0; i < cells.length; i++) {
      cells[i].classList.remove("border-notop", "border-nobottom", "border-noleft", "border-noright");
      var roomId = cells[i].dataset.roomid;

        // top
        if(cells[i - mapWidth] && cells[i - mapWidth].dataset.roomid == roomId) {
          cells[i].classList.add("border-notop");
        }

        // bottom
        if(cells[i + mapWidth] && cells[i + mapWidth].dataset.roomid == roomId) {
          cells[i].classList.add("border-nobottom");
        }

        // left
        if((i % mapWidth) != 0 && cells[i - 1] && cells[i - 1].dataset.roomid == roomId) {
          cells[i].classList.add("border-noleft");
        }

        // right
        if(((i + 1) % mapWidth) != 0 && cells[i + 1] && cells[i + 1].dataset.roomid == roomId) {
          cells[i].classList.add("border-noright");
        }
    }
  }
}

/*
Download Map

Retrieve the user's layout from the server.
*/
function downloadMap() {
  var parameters = {function: "downloadLayout"};
  newAjaxRequest(parameters, mapDownloaded);
}

/*
Map Downloaded

Process the layout downloaded from downloadMap()
*/
function mapDownloaded() {
  mapData = JSON.parse(this.response);
  oldMapData = JSON.parse(this.response);

  createLayoutFromLocal();

  if(!editMode) {
    layoutReel.children[0].classList.remove("raised"); // starts showing first level.
    updateButtons();

    // Hide level buttons if only one level
    if(layoutReel.children.length == 1) {
      nextButton.style.display = "none";
      prevButton.style.display = "none";
    }
  }
}

/*
Create Layout From Local

Renders the HTML structure of the layout view from the downloaded JSON object
*/
function createLayoutFromLocal() {
  layoutReel.innerHTML = "";

  // For each level
  for(var li = 0; li < mapData.levels.length; li++) {
    var level = document.createElement("DIV");
    level.classList.add("layout-level");
    level.classList.add("raised");
    level.dataset.level = li;

    var cellContainer = document.createElement("DIV");
    cellContainer.classList.add("cell-container");

    // For each cell
    for(var i = 0; i < mapData.levels[li].length; i++) {
      var label = "";
      var extraClasses = "";
      if(mapData.levels[li][i].isLabel) {
        label = mapData.meta.rooms[mapData.levels[li][i].roomid - 1];
        extraClasses += " is-label";
      }
      cellContainer.innerHTML = cellContainer.innerHTML + "<div class='cell" + extraClasses + "' data-cellid=" + i + " data-roomid=" + mapData.levels[li][i].roomid + " onclick='cellClicked(event)'><p class=\"label\">" + label + "</p></div>";
    }

    level.appendChild(cellContainer);
    layoutReel.appendChild(level);

    // Editor controls
    var controls = document.createElement("DIV");
    controls.classList.add("level-controls");

    controls.innerHTML =  `<button class='background-image green-button remove-level' onClick='removeLevel(event)'></button>
                          <button class='background-image green-button shift-up' onclick='shiftLevel(event, -1)'></button>
                          <button class='background-image green-button shift-down' onclick='shiftLevel(event, 1)'></button>
                          <button class='background-image green-button add-row' onclick='addRow(event)'></button>
                          <button class='background-image green-button remove-row' onclick='removeRow(event)'></button>`;

    level.appendChild(controls);
  }

  updateMapBorders();
  updateEditShiftButtons();

  roomOptions = "<option>None</option>";
  for(var i = 0; i < mapData.meta.rooms.length; i++) {
    if(mapData.meta.rooms[i] != null) {
      roomOptions += `<option>${mapData.meta.rooms[i]}</option>`;
    }
  }

  if(editMode) {
    updateRoomList();
  }

  for(var i = 0; i < mapData.devices.length; i++) {
    if(!loadedExtras.includes(mapData.devices[i].typeid)) {
      injectDeviceSciptsAndStyles(mapData.devices[i].camelName);
      loadedExtras.push(mapData.devices[i].typeid);
    }
  }

  populatePopupRoomNames();

  if(layoutReel.children.length == 1) {
    layoutReel.querySelector(".remove-level").disabled = true;
  }

  if(selectedCellId) {
    highlightSelectedCells();
    updatePopupContent();
  }

  checkForChanges();

  if(!editMode) {
    onLayoutLoad();
    addModuleRefreshHook(document.querySelector("[data-moduletype='layout']").dataset.moduleid);
  }
}

function cellClicked(e) {
  unhighlightAllCells();

  // If it's a new room, close last room's device menu
  if(selectedRoom != e.target.dataset.roomid) {
    popupUnselectDevice();
  }

  selectedCellId = e.target.dataset.cellid;
  selectedRoom = e.target.dataset.roomid;
  selectedLevel = e.target.parentElement.parentElement.dataset.level;

  if(!editMode) {
    // If clicked on cell is not set
    if(selectedRoom == 0) {
      closePopup();
      return;
    }
  }

  highlightSelectedCells();
  openPopup();
  updatePopupPosition(e);
  updatePopupContent();


}

/*
Next Level

Navigation control between levels.
*/
function nextLevel() {
  layoutReel.children[currentLevel].classList.add("lowered");
  layoutReel.children[currentLevel + 1].classList.remove("raised");
  currentLevel++;
  updateButtons();
  closePopup();
}

/*
Previous Level

Navigation control between levels.
*/
function previousLevel() {
  layoutReel.children[currentLevel].classList.add("raised");
  layoutReel.children[currentLevel - 1].classList.remove("lowered");
  currentLevel--;
  updateButtons();
  closePopup();
}

/*
Shift Level

Requires:
e - Click event.
direction - 1 or -1, to signify going up or down.

Navigation control between levels.
*/
function shiftLevel(e, direction) {
  var currentLevel = parseInt(e.target.parentElement.parentElement.dataset.level);
  var currentLevelData = mapData.levels[currentLevel];

  mapData.levels[currentLevel] = mapData.levels[currentLevel + direction];
  mapData.levels[currentLevel + direction] = currentLevelData;
  createLayoutFromLocal();
  closePopup();
}

/*
Update Buttons

Update disabled and visibility states of controls
*/
function updateButtons() {
  nextButton.disabled = false;
  prevButton.disabled = false;

  if(currentLevel == 0) {
    prevButton.disabled = true;
  }

  if(currentLevel == layoutReel.children.length - 1) {
    nextButton.disabled = true;
  }
}

/*
Update Edit Shift Buttons

A modified updateButtons() for the edit page
*/
function updateEditShiftButtons() {
  var levels = layoutReel.children;

  // Reset shift buttons
  var allShiftButtons = document.querySelectorAll(".shift-up, .shift-down");
  for(var i = 0; i < allShiftButtons.length; i++) {
    allShiftButtons[i].disabled = false;
  }

  // Disable top and bottom shift buttons.
  layoutReel.children[0].querySelector(".shift-up").disabled = true;
  layoutReel.children[layoutReel.children.length - 1].querySelector(".shift-down").disabled = true;

  // Hide level buttons if only one level
  if(levels.length == 1) {
    document.querySelector(".shift-up").style.display = "none";
    document.querySelector(".shift-down").style.display = "none";
  }

  // Reset remove row buttons.
  var allRemoveRowButtons = document.querySelectorAll(".remove-row");
  for(var i = 0; i < allRemoveRowButtons.length; i++) {
    allRemoveRowButtons[i].disabled = false;
  }

  // Disable remove row buttons if level at minimum height.
  for(var i = 0; i < levels.length; i++) {
    var height = levels[i].querySelector(".cell-container").children.length / mapData.meta.width;
    if(height == 1) {
      levels[i].querySelector(".remove-row").disabled = true;
    }
  }
}

/*
Save Layout

Saves the local copy of the user layout to their database profile
*/
function saveLayout() {
  if(editMode) {
    saveButton.disabled = true;
  }
  needsSaving = false;

  var layoutString = JSON.stringify(mapData);

  var parameters = {function: "saveLayout", layout: layoutString};
  newAjaxRequest(parameters, null);
}

/*
Check For Changes

Compares current layout to what is on the server and decides if the save button should be clickable.
*/
function checkForChanges() {
  if(JSON.stringify(mapData) === JSON.stringify(oldMapData)) {
    needsSaving = false;
    saveButton.disabled = true;
  } else {
    needsSaving = true;
    saveButton.disabled = false;
  }
}

/*
Check For Changes

When user presses the 'plus' button in the navbar. Adds another level to their layout.
*/
function addLevel() {
  mapData.levels.push([]);

  var levelWidth = mapData.meta.width;

  for(var i = 0; i < levelWidth; i++) {
    mapData.levels[mapData.levels.length - 1].push({roomid:0,isLabel:false});
  }

  createLayoutFromLocal();
}

/*
Remove Level

Requires:
e - Click event.

Removes the current level from the user's layout
*/
function removeLevel(e) {
  var level = parseInt(e.target.parentElement.parentElement.dataset.level);
  mapData.levels.splice(level, 1);
  closePopup();
  createLayoutFromLocal();
}

/*
Add Row

Requires:
e - Click event.

Adds a row to the current level
*/
function addRow(e) {
  var level = parseInt(e.target.parentElement.parentElement.dataset.level);
  var levelWidth = mapData.meta.width;

  for(var i = 0; i < levelWidth; i++) {
    mapData.levels[level].push({});
    mapData.levels[level][mapData.levels[level].length - 1].roomid = 0;
  }

  createLayoutFromLocal();
}

/*
Remove Row

Requires:
e - Click event.

Removes a row from the current level
*/
function removeRow(e) {
  var level = parseInt(e.target.parentElement.parentElement.dataset.level);
  var levelWidth = mapData.meta.width;

  for(var i = 0; i < levelWidth; i++) {
    mapData.levels[level].pop();
  }

  closePopup();
  createLayoutFromLocal();
}

/*
Get Device From ID

Requires:
id - Device ID.

Gets the device JSON.
*/
function getDeviceFromId(id) {
  return mapData.devices.filter(devices => devices.deviceid === parseInt(id))[0];
}

/*
Inject Device Script And Styles

Requires:
device - device name (e.g. smartLedLamp)

Gets the relevant CSS and Scripts for a specific device and loads them in.
*/
function injectDeviceSciptsAndStyles(device) {
  var script = document.createElement('SCRIPT');
  script.type = 'text/javascript';
  script.src = 'devices/' + device + '/scripts.js';
  document.querySelector("body").appendChild(script);

  var link = document.createElement('LINK');
  link.rel  = 'stylesheet';
  link.type = 'text/css';
  link.href = 'devices/' + device + '/styles.css';
  link.media = 'all';
  document.querySelector("body").appendChild(link);
}

/*
Add New Device

Requires:
deviceCamelName - the camel name of the device

Downloads initial data for specified device
*/
function addNewDevice(deviceCamelName) {
  var parameters = {file: "/devices/" + deviceCamelName + "/data.json"}
  newAjaxFileRequest(parameters, newDeviceDownloaded);
}

/*
New Device Downloaded

After a device's initial data gets downloaded, the new device is instantiated and added to the user's layout.
*/
function newDeviceDownloaded() {
  var newDevice = JSON.parse(this.response);
  lastIdUsed++;
  newDevice.deviceid = lastIdUsed;
  mapData.devices.push(newDevice);
  createLayoutFromLocal();

  var parameters = {function: "setLastDeviceId", id: lastIdUsed};
  newAjaxRequest(parameters, null);
}

/*
Device Count Downloaded

Gets and saves the 'lastDeviceIdUsed' data.
*/
function deviceCountDownloaded() {
  lastIdUsed = this.response;
}

/*
On Layout Load

Sizes the layout to fit in modules
*/
function onLayoutLoad() { // Do for biggest level (multiple heights)
  var tallestLevel = 0;
  for(var i = 0; i < layoutReel.children.length; i++) {
    var numCells = layoutReel.children[i].querySelector(".cell-container").children.length;

    if(numCells > tallestLevel) {
      tallestLevel = numCells;
    }
  }

  var height = tallestLevel / mapData.meta.width;
  var desiredPHeight = document.querySelector(".module[data-moduletype='layout']").offsetHeight;
  var newCellSize = desiredPHeight / height;
  var newWidth = newCellSize * mapData.meta.width;

  document.querySelector(".layout-wrapper").style.width = newWidth + "px";
  layoutReel.style.height = desiredPHeight;

  if(needsRefresh) {
    needsRefresh = false;
    modulesResize();
  }
}

/*
Layout_Resize

Called on screen-resize (for module view)
*/
function layout_resize(module) {
  onLayoutLoad();
}

/*
Layout_Init

Called on document load (for module view)
*/
function layout_init(module, moduleData) {
  layoutInit();
}

function layoutInit() {
  if(document.querySelector("body").classList.contains("layout-editor-body")) {
    editMode = true;
  } else {
    editMode = false;
  }

  layoutReel = document.querySelector(".layout-reel");
  if(editMode) {
    initRoomList();
  }
  initPopup();
  downloadMap();

  nextButton = document.querySelector(".next");
  prevButton = document.querySelector(".prev");
  saveButton = document.querySelector(".editor-hotbar-save");
  currentLevel = 0;
  needsSaving = false;
  loadedExtras = [];

  var parameters = {function: "getLastDeviceId"};
  newAjaxRequest(parameters, deviceCountDownloaded);

  if(!editMode) {
    popupRoomName.disabled = true;
  }
}

// Intercepts the user leaving the page with unsaved changes.
window.addEventListener("beforeunload", function (e) {
    if(needsSaving) {
      var confirmationMessage = 'You have unsaved changes';
      (e || window.event).returnValue = confirmationMessage; // Gecko + IE
      return confirmationMessage; // Gecko + Webkit, Safari, Chrome etc.
    } else {
      return;
    }
});

document.addEventListener("DOMContentLoaded", function() {
  if(document.querySelector("body").classList.contains("layout-editor-body")) {
    editMode = true;
    layoutInit();
  }
});
