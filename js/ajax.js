"use strict";

/*
New Ajax Request

Requires:
parameters -> function - What PHP function to call.
Optional:
callback - Function to call after receiving response.
parameters -> * - data that may be used by the called function.

This function calls a PHP function from the functions.php file, usually to access the database.
*/
function newAjaxRequest(parameters, callback) {
  var xmlhttp = new XMLHttpRequest();
  xmlhttp.addEventListener("load", callback);
  xmlhttp.open("POST", "/functions.php");
  xmlhttp.setRequestHeader("Content-Type", "application/json");
  xmlhttp.send(JSON.stringify(parameters));
}

/*
New Ajax File Request

Requires:
parameters -> file - The path to the file.
Optional:
callback - Function to call after receiving response.

This function retreives any file from the web server.
*/
function newAjaxFileRequest(parameters, callback) {
  var xmlhttp = new XMLHttpRequest();
  xmlhttp.addEventListener("load", callback);
  xmlhttp.open("POST", parameters.file);
  xmlhttp.setRequestHeader("Content-Type", "application/json");
  xmlhttp.send();
}
