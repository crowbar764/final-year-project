// https://css-tricks.com/how-to-make-a-line-chart-with-css/   (modified)
"use strict";

var graphElement, topMostPoint, currentGraph;
var moduleData = {};
var currentGraph = 0;

function formatLineChartData(values) {

  // divide chart size by total number of points to get length of triangle base. That becomes the left offset for each new point
  // subtract previous point height from new point height to get the rise of the triangle. That becomes the bottom offset for the new point.
  // use base squared + rise squared to find the length of the hypotenuse. That becomes the width of the line to draw.
  // use Math.asin(base / hypotenuse) [then convert the radians to degrees] to find the degree angle to rotate the line to.
  // Multiply the rotation angle by -1 if it needs to rise to meet the next point.

  var x = graphElement.offsetWidth;
  var y = graphElement.offsetHeight - 10;

  var base = x / (values.length - 1);

  var sortedValues = sortValues([...values]);

  topMostPoint = Math.ceil(sortedValues[0].value / 10) * 10;
  let leftOffset = 0; // padding for left axis labels
  let nextPoint = 0;
  let rise = 0;
  let cssValues = [];

  for (var i=0, len=values.length-1; i<len; i++) {

    var currentValue = {
      left: 0,
      bottom: 0,
      hypotenuse: 0,
      angle: 0,
      value: 0
    };

    currentValue.value = values[i].value;
    currentValue.ylabel = values[i].label;
    currentValue.left = leftOffset;
    leftOffset += base;

    currentValue.bottom = y * (currentValue.value / topMostPoint);
    nextPoint = y * (values[i+1].value / topMostPoint);

    rise = currentValue.bottom - nextPoint;
    currentValue.hypotenuse = Math.sqrt((base * base) + (rise * rise));
    currentValue.angle = radiansToDegrees(Math.asin(rise / currentValue.hypotenuse));

    cssValues.push(currentValue);
  }

  var lastPoint = {
    left: leftOffset,
    bottom: y * (values[values.length - 1].value / topMostPoint),
    hypotenuse: 0,
    angle: 0,
    value: values[values.length - 1].value,
    ylabel: values[values.length - 1].label
  };

  cssValues.push(lastPoint);

  return cssValues;
}

var sortValues = values => values.sort((a, b) => b.value - a.value)

var radiansToDegrees = (rads) => rads * (180 / Math.PI)

var sum = (total, value) => total + value


function render(chart) {
  graphElement = chart.querySelector('.graph-content');
  graphElement.querySelector("ul").innerHTML = "";
  var graphY = chart.querySelector(".graph-y");
  graphY.innerHTML = "";
  var data = formatLineChartData(moduleData[currentGraph].graphdata);

  chart.querySelector('.label-limit').innerHTML = Math.round(topMostPoint);
  chart.querySelector('.label-half').innerHTML = Math.round(topMostPoint / 2);

  data.forEach((item) => {
    let markup = createListItem(item);
    let listItem = document.createElement("li");
    listItem.style.cssText = `--x: ${item.left}px; --y: ${item.bottom}px`;
    listItem.innerHTML = markup;
    graphElement.querySelector("ul").appendChild(listItem);
    graphY.innerHTML += `<p>${item.ylabel}</p>`;
  });

  // Calculating how much content to show on graph-y

  var yLabels = graphY.children;
  var yLabelsWidth = 0;

  for(var i = 0; i < yLabels.length; i++) {
    yLabelsWidth += yLabels[i].offsetWidth;
  }

  if(yLabelsWidth > graphY.offsetWidth) {
    graphY.classList.add("compact");
  }
}

function createListItem(item) {
  var colour = "red";
  if(item.angle > 0) {
    colour = "green";
  }

  return `
  <div class="data-point" data-value="${item.value}"></div>
  <div class="line-segment graph-${colour}" style="--hypotenuse: ${item.hypotenuse}; --angle:${item.angle};"></div>
  `;
}

function graph_init(graph, incomingData) {
  currentGraph = graph.dataset.moduleid;
  moduleData[currentGraph] = incomingData;
  render(graph);
  currentGraph++;
}

function graph_resize(graph) {
  currentGraph = graph.dataset.moduleid;
  render(graph);
}
