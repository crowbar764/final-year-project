<?php
  include 'templates/header.php';
  include 'templates/access-check.php';
  ?>
  
  <script> document.querySelector("body").classList.add("dashboard-body"); </script>

  <?php
  include 'templates/modules.php';
  include 'templates/layout-popup.php';
  include 'templates/footer.php';
