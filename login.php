<?php
  include 'templates/header.php';

  if (isset($_SESSION['loggedin'])) {
    header('Location: index.php');
    exit;
  }
?>

<script src="js/login.js"></script>
<div class="form-container">
  <div class="form-selection-bar">
    <button class="form-selection-login" onclick="selectLoginForm()" disabled>Log in</button>
    <button class="form-selection-signup" onclick="selectSignupForm()">Sign up</button>
  </div>
  <div class="form-wrapper">
    <form class="login-form" onsubmit="loginAttempt(event)">
      <div>
        <label for="username">Username</label>
        <input type="text" name="username" class="username" required>

        <label for="password">Password</label>
        <input type="password" name="password" class="password" required>
      </div>
      <div>
        <input type="submit" value="Login" class="green-button">
        <p class="error"></p>
      </div>
    </form>
    <form class="signup-form" onsubmit="signupAttempt(event)">
      <div>
        <label for="username">Username</label>
        <input type="text" name="username" class="username" required>

        <label for="password">Password</label>
        <input type="password" name="password" class="password" required>

        <label for="password2">Confirm password</label>
        <input type="password" name="password2" class="password2" required>
      </div>
      <div>
        <input type="submit" value="Sign up" class="green-button">
        <p class="error"></p>
      </div>
    </form>
  </div>
</div>
